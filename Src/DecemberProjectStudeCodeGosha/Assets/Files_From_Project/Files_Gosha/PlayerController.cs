﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
	public static PlayerController _playerController;
	

	[Header("Move")]
	public float speed = 10f;
	public float runSpeed = 20f;
	public float JumpForce = 300f;


	[HideInInspector]
	public Animator playerAnim;
	public GameObject cam;
	[SerializeField] Transform pivotCam;
	[HideInInspector]
	public Camera camMain;

	float turnSmoothVelocity;
	public float turnSmoothTime = 0.1f;
	//public CharacterController playerController;

	[Header("Mouse Rotation")]
	public float sensitive = 3f;
	public float minX = -360;
	public float maxX = 360;
	public float minY = -60;
	public float maxY = 60;

	Quaternion originalRot;
	float RotX = 0;
	float RotY = 0;
	

	[HideInInspector]
	public float hor, ver;

	public Vector3 offset = new Vector3 (0, 2, 0);

	Quaternion xQuaternion;
	Quaternion yQuaternion;

	[HideInInspector]
	public Rigidbody _rb;

	public Ray ray;
	public RaycastHit hit;
	[Header("Controls")]
	public KeyCode JumpKey = KeyCode.JoystickButton3;

	public float minDistance = 0.2f,
		maxDistance = 3f,
		smooth = 10f;
	Vector3 dollyDir;
	public Vector3 dollyDirAdjusted;
	public float distance;


	public static bool mouseControl = true;
	public bool moveControl = true,
		strafeMode = false,
		runMode = false;
    void Awake()
	{
		_playerController = this;
		_rb = GetComponent<Rigidbody>();
		playerAnim = GetComponent<Animator>();
	}
    void Start()
	{
		if (_rb) {
			_rb.freezeRotation = true;
		}
		camMain = cam.transform.GetChild(0).GetComponent<Camera>();
		originalRot = transform.localRotation;
		dollyDir = camMain.transform.localPosition.normalized;
		distance = camMain.transform.localPosition.magnitude;
	}
    void LateUpdate()
	{
		Jump();
		
		if (mouseControl)
		{
			RotX += Input.GetAxis("Mouse X") * sensitive;
			RotY += Input.GetAxis("Mouse Y") * sensitive;

			Debug.Log("");

			RotX = RotX % 360;
			RotY = RotY % 360;

			RotX = Mathf.Clamp(RotX, minX, maxX);
			RotY = Mathf.Clamp(RotY, minY, maxY);

			xQuaternion = Quaternion.AngleAxis(RotX, Vector3.up);
			yQuaternion = Quaternion.AngleAxis(RotY, Vector3.left);
		}
		CollisionCam();
	}
	void FixedUpdate()
	{
		if (moveControl)
		{
			if (Input.GetKey(KeyCode.LeftShift))
			{
				runMode = true;
			}
			if (Input.GetKeyUp(KeyCode.LeftShift))
			{
				runMode = false;
			}
			cam.transform.position = pivotCam.transform.position + offset;
			playerAnim.SetBool("Strafe", strafeMode);
			if (!strafeMode)
			{
				MovementLogic();
				cam.transform.localRotation = Quaternion.Lerp(cam.transform.localRotation, originalRot * xQuaternion * yQuaternion, sensitive * Time.deltaTime * 2);
			}
		}
	}
	void Jump()
    {
		ray = new Ray(transform.localPosition, Vector3.down);
		if (Physics.Raycast(ray, out hit, 0.75f))
		{
			Debug.Log("false");
			playerAnim.SetBool("Jump", false);
			Debug.DrawRay(transform.localPosition, Vector3.down * 0.75f, Color.red);
			if (Input.GetKeyDown(JumpKey) || Input.GetKeyDown(KeyCode.Space))
			{
				_rb.AddForce(Vector3.up * Time.deltaTime * JumpForce, ForceMode.Impulse);
			}
		}
		else
		{
			Debug.Log("true");
			playerAnim.SetBool("Jump", true);
			Debug.DrawRay(transform.localPosition, Vector3.down * 0.2f, Color.blue);
		}
	}
	void CollisionCam()
	{
		Vector3 desiredCamPos = camMain.transform.TransformPoint(dollyDir * maxDistance);
		RaycastHit hit;

		if (Physics.Linecast(camMain.transform.position, desiredCamPos, out hit))
		{
			distance = Mathf.Clamp((hit.distance * 0.9f), minDistance, maxDistance);
		}
		else
		{
			distance = maxDistance;
		}
	}
	void MovementLogic()
	{
			hor = Input.GetAxis("Horizontal");
			ver = Input.GetAxis("Vertical");
			if (hor == 0 && ver == 0)
			{
				playerAnim.SetBool("Forward", false);
			}
            else
			{
				playerAnim.SetBool("Forward", true);
			}
			hor = Input.GetAxis("Horizontal");
			ver = Input.GetAxis("Vertical");

			Vector3 direction = new Vector3(hor, 0, ver).normalized;

		if (direction.magnitude >= 0.1f)
		{
			float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + camMain.transform.eulerAngles.y;
			float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
			transform.rotation = Quaternion.Euler(0f, angle, 0f);
			if (!runMode)
			{
				transform.Translate(Vector3.forward * speed * Time.deltaTime);
			}
			if (runMode)
			{
				transform.Translate(Vector3.forward * runSpeed * Time.deltaTime);
			}
		}
	}
	public void StrafeMode()
	{
		if (moveControl)
		{
			hor = Input.GetAxis("Horizontal");
			ver = Input.GetAxis("Vertical");
			playerAnim.SetFloat("Hor", hor);
			playerAnim.SetFloat("Ver", ver);
			if (runMode)
			{
				
				transform.Translate(new Vector3(hor, 0, ver) * runSpeed * Time.deltaTime);
				
				
			}
			else
				transform.Translate(new Vector3(hor, 0, ver) * speed * Time.deltaTime);
		}
	}
}