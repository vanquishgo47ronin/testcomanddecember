using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private Rigidbody _gamer;
    [SerializeField] private float _speed;
    private float moveX, moveY;

    // Update is called once per frame
    void Update()
    {
        moveX = Input.GetAxis("Horizontal");
        moveY = Input.GetAxis("Vertical");
        transform.Translate(Vector3.right * _speed * moveX * Time.deltaTime);
        transform.Translate(Vector3.forward * _speed * moveY * Time.deltaTime);

        // ������.
        transform.Rotate(0, Input.GetAxis("Mouse X") * _speed, 0);
    }
}
